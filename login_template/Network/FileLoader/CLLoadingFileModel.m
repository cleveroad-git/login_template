//
//  CLLoadingFileModel.m
//  login_template
//
//  Created by Gromov on 19.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLLoadingFileModel.h"

@implementation CLLoadingFileModel

- (instancetype)initWithFilePath:(NSString *)fileUrl completionBlock:(CompletionBlock)completionBlock
{
    self = [super init];
    if (self) {
        self.fileUrl = fileUrl;
        self.completionBlock = completionBlock;
    }
    return self;
}

@end
