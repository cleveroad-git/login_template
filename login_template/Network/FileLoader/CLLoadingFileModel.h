//
//  CLLoadingFileModel.h
//  login_template
//
//  Created by Gromov on 19.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"

typedef NS_ENUM(NSInteger, CLFileType) {
    CLFileTypeImage,
    CLFileTypeData
};

@interface CLLoadingFileModel : NSObject

@property (nonatomic, strong) NSString * fileUrl;
@property (copy) CompletionBlock completionBlock;
@property (readwrite) CLFileType fileType;
@property (readwrite) int numberOfAttempts;
@property (readwrite) BOOL isLoading;

- (instancetype)initWithFilePath:(NSString *)fileUrl  fileType:(CLFileType)fileType completionBlock:(CompletionBlock)completionBlock;

@end
