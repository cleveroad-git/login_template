//
//  FileLoader.m
//  login_template
//
//  Created by Gromov on 19.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "FileLoader.h"
#import <AFNetworking/AFNetworking.h>

@interface FileLoader () {
    NSMutableArray * loadingQueue;
}

@end

@implementation FileLoader

- (void)loadFileByUrl:(NSString *)fileUrl fileType:(CLFileType)fileType completionBlock:(CompletionBlock)completion
{
    CLLoadingFileModel * model = [[CLLoadingFileModel alloc] initWithFilePath:fileUrl fileType:fileType completionBlock:completion];
    [loadingQueue addObject:model];
//    [self startTracker];
    if (loadingQueue.count == 1) {
        [self loadFileByLoadingModel:model];
    }
}

- (void)loadFileByLoadingModel:(CLLoadingFileModel *)model
{
    if (!model || model.isLoading) {
//        if (!imageModel) {
//            [self stopTracker];
//        }
        return;
    }
    model.isLoading = YES;
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.fileUrl]]];
    if (model.fileType == CLFileTypeImage) {
        requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
    }
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [loadingQueue removeObject:model];
        model.completionBlock(responseObject, nil);
        [self loadFileByLoadingModel:loadingQueue.firstObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        model.isLoading = NO;
        if ([AFNetworkReachabilityManager sharedManager].reachable)
        {
            if (++model.numberOfAttempts == 3) {
                [loadingQueue removeObject:model];
                model.completionBlock(nil, error);
            }
            [self loadFileByLoadingModel:loadingQueue.firstObject];
        }
    }];
    [requestOperation start];
}


@end
