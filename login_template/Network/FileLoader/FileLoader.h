//
//  FileLoader.h
//  login_template
//
//  Created by Gromov on 19.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"
#import "CLLoadingFileModel.h"

@interface FileLoader : NSObject

- (void)loadFileByUrl:(NSString *)fileUrl fileType:(CLFileType)fileType completionBlock:(CompletionBlock)completion;

@end
