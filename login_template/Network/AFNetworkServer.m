//
//  AFNetworkServer.m
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "AFNetworkServer.h"
#import "JSONResponseSerializerWithData.h"
#import <AFNetworking/AFNetworking.h>
#import "CLNetworkConstants.h"

@implementation AFNetworkServer

- (instancetype)init
{
    self = [super init];
    if (self) {
        AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
        [manager startMonitoring];
        __weak typeof (self) weakSelf = self;
        [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            weakSelf.networkIsReachable = status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN;
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationNetworkIsReachable object:self];
//            if (weakSelf.networkIsReachable) {
//                [weakSelf repeatRequestsFinishedWithError];
//            }
        }];
    }
    return self;
}

#pragma mark -
#pragma mark Registration

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    [self.requestOperationManager.requestSerializer setValue:XApiKey forHTTPHeaderField:@"X-Api-Key"];
    [self.requestOperationManager POST:METHOD_REG parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@", responseObject);
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        NSError * newError = error = [self parseError:error];
        completion(nil, newError);
    }];
}

- (void)registrationBySocialWithParams:(NSDictionary *)params method:(NSString *)method completionBlock:(CompletionBlock)completion
{
    [self.requestOperationManager.requestSerializer setValue:XApiKey forHTTPHeaderField:@"X-Api-Key"];
    [self.requestOperationManager POST:method parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@", responseObject);
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        NSError * newError = error = [self parseError:error];
        completion(nil, newError);
    }];
    
//    email = "nickgromov73@gmail.com";
//    firstName = Nick;
//    id = 19;
//    lastName = Gromov;
//    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjE5LCJpYXQiOjE0NDc5Mjg4MjN9.HgJWx3Sd_CgnZPirw25aVV_lqHAQquI3eH7evdDxh8c";
    
}

- (void)getInstagramUser:(NSString *)accessToken completionBlock:(CompletionBlock)completion
{
    NSString * urlString = [NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/?access_token=%@",accessToken];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    NSURLSessionDataTask * dataTask =[self.requestOperationManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        completion(responseObject, error);
    }];
    [dataTask resume];
}

- (void)loginWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    [self.requestOperationManager.requestSerializer setValue:XApiKey forHTTPHeaderField:@"X-Api-Key"];
    [self.requestOperationManager POST:METHOD_AUTH parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSLog(@"%@", responseObject);
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        NSError * newError = error = [self parseError:error];
        completion(nil, newError);
    }];
}


#pragma mark - Parse Error

- (NSError *)parseError:(NSError *)error
{
    id data = error.userInfo[JSONResponseSerializerWithDataKey];
    id response = data ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
    NSError * newError = [NSError errorWithDomain:@"Server" code:error.code userInfo:response];
    NSLog(@"parseError - %@ \n %@",error , newError);
    return newError;
}


#pragma mark - AFHTTPSessionManager

- (AFHTTPSessionManager *)requestOperationManager {
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        NSLog(@"BASE URL = %@",BASE_URL);
        manager = [[AFHTTPSessionManager alloc]
                   initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        //        [manager.requestSerializer setValue:AUTH_TOKEN forHTTPHeaderField:@"Authorization"];
        manager.responseSerializer = [JSONResponseSerializerWithData serializer];// [AFJSONResponseSerializer serializer];
    });
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    return manager;
}

@end
