//
//  ImageUploader.h
//  login_template
//
//  Created by Gromov on 17.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"

@class AFHTTPSessionManager;

@interface ImageUploader : NSObject

- (void)releaseImageUploader;
- (instancetype)initWithAFSessionManager:(AFHTTPSessionManager *)requestOperationManager;

- (void)uploadImageData:(NSData *)imageData requestParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;
- (void)uploadImageByFilePath:(NSString *)filePath requestParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;

@end
