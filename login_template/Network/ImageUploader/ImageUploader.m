//
//  ImageUploader.m
//  login_template
//
//  Created by Gromov on 17.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "ImageUploader.h"
#import <AFNetworking/AFNetworking.h>
#import "CLUploadingImageModel.h"


NSString * const DIRECTORY_NAME = @"UnsentImages";
NSString * const uploadImagePath      = @"/uploadImagePath/";

@interface ImageUploader () {
    AFHTTPSessionManager * _requestOperationManager;
    NSMutableArray * uploadingQueue;
    NSTimer * trackerTimer;
}

@end

@implementation ImageUploader

- (void)releaseImageUploader
{
    [self stopTracker];
}

- (instancetype)initWithAFSessionManager:(AFHTTPSessionManager *)requestOperationManager
{
    self = [super init];
    if (self) {
        _requestOperationManager = requestOperationManager;
        uploadingQueue = [NSMutableArray array];
    }
    return self;
}

#pragma mark -
#pragma mark NSTimer

- (void)startTracker
{
    if (!trackerTimer) {
        trackerTimer = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                        target:self
                                                      selector:@selector(trackerTimerDidFired:)
                                                      userInfo:nil
                                                       repeats:YES];
        [trackerTimer fire];
    }
}

- (void)stopTracker
{
    if (trackerTimer) {
        [trackerTimer invalidate];
        trackerTimer = nil;
    }
}

- (void)trackerTimerDidFired:(NSTimer *)timer
{
    if ([AFNetworkReachabilityManager sharedManager].reachable && ((CLUploadingImageModel *)uploadingQueue.firstObject).isUploading == NO)
    {
        [self uploadImageByUploadingImageModel:uploadingQueue.firstObject];
    }
}

#pragma mark -
#pragma mark UploadImage

- (void)uploadImageData:(NSData *)imageData requestParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    NSString * filePath = [self writeDataToFile:imageData];
    if (!filePath) {
        //ERROR
        return;
    }
    [self uploadImageByFilePath:filePath requestParams:params  completionBlock:completion];
}

- (void)uploadImageByFilePath:(NSString *)filePath requestParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    CLUploadingImageModel * model = [[CLUploadingImageModel alloc] initWithFilePath:filePath requestParameters:params completionBlock:completion];
    [uploadingQueue addObject:model];
    [self startTracker];
    if (uploadingQueue.count == 1) {
        [self uploadImageByUploadingImageModel:model];
    }
}

- (void)uploadImageByUploadingImageModel:(CLUploadingImageModel *)imageModel
{
    if (!imageModel || imageModel.isUploading) {
        if (!imageModel) {
            [self stopTracker];
        }
        return;
    }
    imageModel.isUploading = YES;
    [_requestOperationManager POST:uploadImagePath
                        parameters:imageModel.requestParameters
         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
             NSError * error;
             [formData appendPartWithFileURL:[NSURL fileURLWithPath:imageModel.localFilePath] name:@"image" fileName:@"image.jpeg" mimeType:@"image/jpeg" error:&error];
             if (error) {
                 //
             }
         } success:^(NSURLSessionDataTask *task, id responseObject) {
             if (responseObject)
             {
                 [self removeDataAtPath:imageModel.localFilePath];
                 [uploadingQueue removeObject:imageModel];
                 imageModel.completionBlock(responseObject, nil);
                 [self uploadImageByUploadingImageModel:uploadingQueue.firstObject];
             }
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
             imageModel.isUploading = NO;
             if ([AFNetworkReachabilityManager sharedManager].reachable)
             {
                 if (++imageModel.numberOfAttempts == 3) {
                     [uploadingQueue removeObject:imageModel];
                     imageModel.completionBlock(nil, error);
                 }
                 [self uploadImageByUploadingImageModel:uploadingQueue.firstObject];
             }
         }];
}



- (NSString *)writeDataToFile:(NSData *)imageData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    const int NUMBER_OF_CHARS = 40;
    char data[NUMBER_OF_CHARS];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *dirPath = [documentsPath stringByAppendingPathComponent:DIRECTORY_NAME];
    if (![fm fileExistsAtPath:dirPath]) {
        NSError *error = nil;
        if (![fm createDirectoryAtPath:dirPath
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:&error])
        {
            NSLog(@"Error creating dirrectory: %@", error.localizedDescription);
            abort();
        }
    }
    for (int x=0; x < NUMBER_OF_CHARS; data[x++] = (char)('A' + (arc4random_uniform(26))));
    NSString * fileName = [[NSString alloc] initWithBytes:data length:NUMBER_OF_CHARS encoding:NSUTF8StringEncoding];
    fileName = [fileName stringByAppendingString:@".png"];
    NSString * filePath = [dirPath stringByAppendingPathComponent:fileName];
    if ([imageData writeToFile:filePath atomically:YES]) {
        return filePath;
    }
    return nil;
}

- (BOOL)removeDataAtPath:(NSString *)imagePath
{
    NSError * error = nil;
    BOOL result = [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
    if (error) {
        NSLog(@"removeDataAtPath: - %@", error.userInfo);
    }
    return result;
}

@end
