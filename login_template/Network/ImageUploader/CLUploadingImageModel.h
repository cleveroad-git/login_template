//
//  CLUploadingImageModel.h
//  login_template
//
//  Created by Gromov on 17.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"

@interface CLUploadingImageModel : NSObject

@property (nonatomic, strong) NSString * localFilePath;
@property (nonatomic, strong) NSDictionary * requestParameters;
@property (copy) CompletionBlock completionBlock;
@property (readwrite) int numberOfAttempts;
@property (readwrite) BOOL isUploading;

- (instancetype)initWithFilePath:(NSString *)localFilePath requestParameters:(NSDictionary *)params completionBlock:(CompletionBlock)completionBlock;

@end
