//
//  CLUploadingImageModel.m
//  login_template
//
//  Created by Gromov on 17.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLUploadingImageModel.h"

@implementation CLUploadingImageModel

- (instancetype)initWithFilePath:(NSString *)localFilePath requestParameters:(NSDictionary *)params completionBlock:(CompletionBlock)completionBlock
{
    self = [super init];
    if (self) {
        self.localFilePath = localFilePath;
        self.requestParameters = params;
        self.completionBlock = completionBlock;
    }
    return self;
}

@end
