//
//  JSONResponseSerializerWithData.h
//  Klubok
//
//  Created by mac on 3/26/15.
//  Copyright (c) 2015 908 Inc. All rights reserved.
//

#import "AFURLResponseSerialization.h"

extern NSString *const JSONResponseSerializerWithDataKey;

@interface JSONResponseSerializerWithData : AFJSONResponseSerializer

@end
