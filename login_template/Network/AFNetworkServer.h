//
//  AFNetworkServer.h
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"

@interface AFNetworkServer : NSObject

@property BOOL networkIsReachable;

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;
- (void)registrationBySocialWithParams:(NSDictionary *)params method:(NSString *)method completionBlock:(CompletionBlock)completion;
- (void)getInstagramUser:(NSString *)accessToken completionBlock:(CompletionBlock)completion;
- (void)loginWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;

@end
