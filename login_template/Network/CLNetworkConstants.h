//
//  CLNetworkConstants.h
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const XApiKey;

FOUNDATION_EXPORT NSString * const BASE_URL;

FOUNDATION_EXPORT NSString * const METHOD_REG;
FOUNDATION_EXPORT NSString * const METHOD_AUTH;

FOUNDATION_EXPORT NSString * const METHOD_REG_FACEBOOK;
FOUNDATION_EXPORT NSString * const METHOD_REG_TWITTER;
FOUNDATION_EXPORT NSString * const METHOD_REG_INSTAGRAM;
FOUNDATION_EXPORT NSString * const METHOD_REG_GOOGLE;
