//
//  CLNetworkConstants.m
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLNetworkConstants.h"

NSString * const XApiKey = @"ccfrrEuh4j84";

NSString * const BASE_URL = @"http://148.251.187.5:3266";

NSString * const METHOD_REG =           @"/api/v1/users";
NSString * const METHOD_AUTH =          @"/api/v1/users/login";

NSString * const METHOD_REG_FACEBOOK  = @"/api/v1/users/facebook";
NSString * const METHOD_REG_TWITTER   = @"/api/v1/users/twitter";
NSString * const METHOD_REG_INSTAGRAM = @"/api/v1/users/instagram";
NSString * const METHOD_REG_GOOGLE    = @"/api/v1/users/google";
