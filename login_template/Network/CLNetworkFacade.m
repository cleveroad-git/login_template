//
//  CLNetworkFacade.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLNetworkFacade.h"
#import "AFNetworkServer.h"
#import "CLNetworkConstants.h"
#import "CLCommon.h"

@interface CLNetworkFacade () {
    AFNetworkServer * afServer;
}

@end

@implementation CLNetworkFacade

+ (id)sharedFacade
{
    static CLNetworkFacade *_manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[super alloc]init];
    });
    return _manager;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        afServer = [[AFNetworkServer alloc] init];
    }
    return self;
}

#pragma mark -
#pragma mark Registration

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    [afServer registrationWithParams:params completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

- (void)registrationViaFacebookWithSocialId:(NSString *)socialId token:(NSString *)token completionBlock:(CompletionBlock)completion
{
    NSDictionary * params = @{@"socialId" : socialId,
                              @"token"    : token
                              };
    [afServer registrationBySocialWithParams:params method:METHOD_REG_FACEBOOK completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

- (void)registrationViaTwitterWithSocialId:(NSString *)socialId
                                     token:(NSString *)token
                                    secret:(NSString *)secret
                           completionBlock:(CompletionBlock)completion
{
    
    NSDictionary * params = @{@"socialId" : socialId,
                              @"token"    : token,
                              @"secret"   : secret
                              };
    [afServer registrationBySocialWithParams:params method:METHOD_REG_TWITTER completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

- (void)registrationViaInstagramWithSocialId:(NSString *)socialId
                                       token:(NSString *)token
                             completionBlock:(CompletionBlock)completion
{
    
    NSDictionary * params = @{@"socialId" : socialId,
                              @"token"    : token
                              };
    [afServer registrationBySocialWithParams:params method:METHOD_REG_INSTAGRAM completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

- (void)getInstagramUser:(NSString *)accessToken completionBlock:(CompletionBlock)completion
{
    [afServer getInstagramUser:accessToken completionBlock:^(id resObject, NSError *error) {
        completion(resObject, error);
    }];
}

- (void)registrationViaGoogleWithSocialId:(NSString *)socialId
                                    token:(NSString *)token
                          completionBlock:(CompletionBlock)completion
{
    NSDictionary * params = @{@"socialId" : socialId,
                              @"token"    : token
                              };
    [afServer registrationBySocialWithParams:params method:METHOD_REG_GOOGLE completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

- (void)loginWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion
{
    [afServer loginWithParams:params completionBlock:^(id resObject, NSError *error) {
        if (error) {
            completion(nil, error);
        }
        else {
            [CLCommon setToken:resObject[@"token"]];
            [CLCommon setUserId:[NSString stringWithFormat:@"%@", resObject[@"id"]]];
            completion(resObject, nil);
        }
    }];
}

@end
