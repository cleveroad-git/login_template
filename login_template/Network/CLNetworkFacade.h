//
//  CLNetworkFacade.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"

#define NETWORK_FACADE [CLNetworkFacade sharedFacade]

@interface CLNetworkFacade : NSObject

+ (id)sharedFacade;

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;

- (void)registrationViaFacebookWithSocialId:(NSString *)socialId token:(NSString *)token completionBlock:(CompletionBlock)completion;

- (void)registrationViaTwitterWithSocialId:(NSString *)socialId
                                     token:(NSString *)token
                                    secret:(NSString *)secret
                           completionBlock:(CompletionBlock)completion;

- (void)registrationViaInstagramWithSocialId:(NSString *)socialId
                                       token:(NSString *)token
                             completionBlock:(CompletionBlock)completion;

- (void)getInstagramUser:(NSString *)accessToken completionBlock:(CompletionBlock)completion;

- (void)registrationViaGoogleWithSocialId:(NSString *)socialId
                                       token:(NSString *)token
                             completionBlock:(CompletionBlock)completion;

- (void)loginWithParams:(NSDictionary *)params completionBlock:(CompletionBlock)completion;

@end
