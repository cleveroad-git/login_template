//
//  UIView+Animations.h
//
//
//

#import "UIKit/UIKit.h"


@interface UIView (Animations)

- (void)showAnimated;
- (void)hideAnimated;
- (void)showAnimatedWithCompletion:(void (^)(void))completion;
- (void)hideAnimatedWithCompletion:(void (^)(void))completion;

- (void)changeSize:(CGSize)newSize withAnimationTime:(CGFloat)time;
- (void)changeFrame:(CGRect)newRect alpha:(float)newAlpha withAnimationTime:(CGFloat)time completion:(void (^)(BOOL finished))completion;

@end




