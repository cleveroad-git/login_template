//
//  UIView+Animations.m
//
//
//

#import "UIView+Animations.h"


@implementation UIView (Animations)

- (void)showAnimated {
  [self showAnimatedWithCompletion:nil];
}

- (void)hideAnimated {
  [self hideAnimatedWithCompletion:nil];
}

- (void)showAnimatedWithCompletion:(void (^)(void))completion {
  //if (self.tag == 0) {
  // Animation
  self.alpha = 0;
  [UIView animateWithDuration:0.3 animations:^{
    self.alpha = 1.0;
  } completion:^(BOOL finished) {
    if (finished && completion)
      completion();
  }];
  //}
  //else
  //  NSLog(@"Warning: Trying to animate view several times.");
  //self.tag++;
}

- (void)hideAnimatedWithCompletion:(void (^)(void))completion
{
  //self.tag = MAX(self.tag-1, 0);
  //if (self.tag == 0) {
  // Animation
  self.alpha = 1.0;
  [UIView animateWithDuration:0.3 animations:^{
    self.alpha = 0;
  } completion:^(BOOL finished) {
    if (finished && completion)
      completion();
  }];
  //}
  //else
  //  NSLog(@"Warning: View will not be hidden for now.");
}

- (void)changeSize:(CGSize)newSize withAnimationTime:(CGFloat)time
{
    self.layer.needsDisplayOnBoundsChange = YES;
    self.contentMode = UIViewContentModeRedraw;
    [UIView animateWithDuration:time animations:^{
        CGRect theBounds = self.bounds;
        CGPoint theCenter = self.center;
        float incrementY = newSize.height - theBounds.size.height;
        float incrementX = newSize.width - theBounds.size.width;
        theBounds.size.height = newSize.height;
        theBounds.size.width = newSize.width;
        theCenter.x += incrementX/2;
        theCenter.y += incrementY/2;
        self.bounds = theBounds;
        self.center = theCenter;
    }];
}

- (void)changeFrame:(CGRect)newRect alpha:(float)newAlpha withAnimationTime:(CGFloat)time completion:(void (^)(BOOL finished))completion
{
    [UIView animateWithDuration:time animations:^{
        self.frame = newRect;
        self.alpha = newAlpha;
    } completion:completion];
}


@end




