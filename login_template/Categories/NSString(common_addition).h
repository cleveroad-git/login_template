//
//  NSString(common_addition).h
//
//
//
//

#import <Foundation/Foundation.h>

@interface NSString (common_addition)

- (NSString *)MD5String;

- (NSString*)SHA1;

- (BOOL)isEmailValid;
//	checks if the inn valid
- (BOOL)isInnValid;
//  trims whitespaces and new line symbols from the beginning an the end
- (NSString *)trim;

//	returns a phone number (if the string can be treated as one, or nil)
- (NSString *)stringAsPhone;
//	returns a login text string (if the string can be treated as one, or nil)
- (NSString *)stringAsLogin;

- (BOOL)isPasswordValid;

- (NSString *)stringAsCurrency;

- (NSString *)capitalizedSentenceString;

@end
