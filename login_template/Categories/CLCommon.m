//
//  CLCommon.m
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLCommon.h"
#import <UIKit/UIKit.h>

NSString * SECRET_TOKEN;
NSString * USER_ID;
NSString * DEVICE_UDID;

@implementation CLCommon

+ (void)initCommon
{
    DEVICE_UDID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    SECRET_TOKEN = [[NSUserDefaults standardUserDefaults] objectForKey:@"secret_token"];
    USER_ID = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
}

+ (void)setToken:(NSString *)token
{
    SECRET_TOKEN = token;
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"secret_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserId:(NSString *)userId
{
    USER_ID = userId;
    [[NSUserDefaults standardUserDefaults] setValue:userId forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
