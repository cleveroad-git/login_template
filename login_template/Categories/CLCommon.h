//
//  CLCommon.h
//  login_template
//
//  Created by Gromov on 13.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * SECRET_TOKEN;
FOUNDATION_EXPORT NSString * USER_ID;
FOUNDATION_EXPORT NSString * DEVICE_UDID;

@interface CLCommon : NSObject

+ (void)initCommon;
+ (void)setToken:(NSString *)token;
+ (void)setUserId:(NSString *)userId;

@end
