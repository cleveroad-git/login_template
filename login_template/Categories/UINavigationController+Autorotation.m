//
//  UINavigationController+Autorotation.m
//
//
// 
//

#import "UINavigationController+Autorotation.h"


@implementation UINavigationController (Autorotation)

- (BOOL)shouldAutorotate {
  return [self.topViewController shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations {
  return [self.topViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
  return [self.topViewController preferredInterfaceOrientationForPresentation];
}

@end





