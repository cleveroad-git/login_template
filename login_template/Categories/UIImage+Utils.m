//
//  UIImage+Utils.m
//
// 
//

#import "UIImage+Utils.h"
#import "Common.h"


@implementation UIImage (Utils)

+ (UIImage *)snapshotOfView:(UIView *)view {
  return [self snapshotOfView:view rect:view.bounds scale:1.0f];
}

+ (UIImage *)snapshotOfView:(UIView *)view rect:(CGRect)contentRect {
  return [self snapshotOfView:view rect:contentRect scale:1.0f];
}

+ (UIImage *)snapshotOfView:(UIView *)view scale:(float)scaleFactor {
  return [self snapshotOfView:view rect:view.bounds scale:scaleFactor];
}

+ (UIImage *)snapshotOfView:(UIView *)view rect:(CGRect)contentRect scale:(float)scaleFactor {
  return [UIImage snapshotOfLayer:view.layer rect:contentRect scale:scaleFactor];
}

+ (UIImage *)snapshotOfLayer:(CALayer *)layer rect:(CGRect)contentRect scale:(float)scaleFactor
{
  float w = scaleFactor * contentRect.size.width;
  float h = scaleFactor * contentRect.size.height;
  
  UIGraphicsBeginImageContext(CGSizeMake(w, h));
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  if (scaleFactor != 1.0f)
    CGContextScaleCTM(context, scaleFactor, scaleFactor);
  CGContextTranslateCTM(context,-contentRect.origin.x,-contentRect.origin.y);
  [layer renderInContext:context];
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)fixOrientation
{
  // No-op if the orientation is already correct
  if (self.imageOrientation == UIImageOrientationUp)
    return self;
  
  // We need to calculate the proper transformation to make the image upright.
  // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
  CGAffineTransform transform = CGAffineTransformIdentity;
  
  switch ((int)self.imageOrientation) {
    case UIImageOrientationDown:
    case UIImageOrientationDownMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
      transform = CGAffineTransformRotate(transform, M_PI);
      break;
      
    case UIImageOrientationLeft:
    case UIImageOrientationLeftMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, 0);
      transform = CGAffineTransformRotate(transform, M_PI_2);
      break;
      
    case UIImageOrientationRight:
    case UIImageOrientationRightMirrored:
      transform = CGAffineTransformTranslate(transform, 0, self.size.height);
      transform = CGAffineTransformRotate(transform, -M_PI_2);
      break;
  }
  
  switch ((int)self.imageOrientation) {
    case UIImageOrientationUpMirrored:
    case UIImageOrientationDownMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;
      
    case UIImageOrientationLeftMirrored:
    case UIImageOrientationRightMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.height, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;
  }
  
  // Now we draw the underlying CGImage into a new context, applying the transform
  // calculated above.
  CGContextRef ctx = CGBitmapContextCreate(NULL,
                                           self.size.width,
                                           self.size.height,
                                           CGImageGetBitsPerComponent(self.CGImage),
                                           0,
                                           CGImageGetColorSpace(self.CGImage),
                                           CGImageGetBitmapInfo(self.CGImage));
  CGContextConcatCTM(ctx, transform);
  switch (self.imageOrientation) {
    case UIImageOrientationLeft:
    case UIImageOrientationLeftMirrored:
    case UIImageOrientationRight:
    case UIImageOrientationRightMirrored:
      // Grr...
      CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage);
      break;
      
    default:
      CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
      break;
  }
  
  // And now we just create a new UIImage from the drawing context
  CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
  UIImage *img = [UIImage imageWithCGImage:cgimg];
  CGContextRelease(ctx);
  CGImageRelease(cgimg);
  return img;
}

- (UIImage *)rotateOnAngle:(float)angleInRadians
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    NSLog(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  CGContextTranslateCTM(context, 0.5 * rect.size.width, 0.5 * rect.size.height);
  CGContextRotateCTM(context, angleInRadians);
  CGContextTranslateCTM(context,-0.5 * rect.size.width,-0.5 * rect.size.height);
  CGContextScaleCTM(context, 1.0, -1.0);
  CGContextTranslateCTM(context, 0.0, -rect.size.height);
  
  CGContextDrawImage(context, rect, self.CGImage);
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)flipVertically
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  CGContextTranslateCTM(context, 0.0, 0.5 * rect.size.height);
  CGContextScaleCTM(context, 1.0, 1.0);
  CGContextTranslateCTM(context, 0.0, -0.5 * rect.size.height);
  
  CGContextDrawImage(context, rect, self.CGImage);
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)resizeToSize:(CGSize)newSize {
  return [self resizeToSize:newSize backgroundColor:nil];
}

- (UIImage *)resizeToSize:(CGSize)newSize backgroundColor:(UIColor *)backgroundColor
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  UIGraphicsBeginImageContext(newSize);
  
  CGRect rect = {0, 0, newSize.width, newSize.height};
  
  if (backgroundColor && !CGColorEqualToColor(backgroundColor.CGColor, [UIColor clearColor].CGColor))
  {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, backgroundColor.CGColor);
    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    CGContextFillRect(context, rect);
  }
  
  [self drawInRect:rect];
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)cropToRect:(CGRect)cropRect
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = CGRectIntersection(CGRectMake(0, 0, w, h), cropRect);
  
  if (CGRectIsNull(rect))
    return nil;
  
  UIGraphicsBeginImageContext(rect.size);
  
  CGRect drawRect = {-MAX(cropRect.origin.x, 0),-MAX(cropRect.origin.y, 0), w, h};
  [self drawInRect:drawRect];
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)previewWithSize:(CGSize)size
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  if ((int)size.width == w && (int)size.height == h)
    return self;
  
  CGSize fillSize = CGSizeScaledToFillSize(CGSizeMake(w, h), size);
  CGRect cropRect = CGRectMake(roundf(0.5*(fillSize.width-size.width)),
                               roundf(0.5*(fillSize.height-size.height)),
                               size.width,
                               size.height);
  
  UIImage *preview = self;
  preview = [preview resizeToSize:fillSize];
  preview = [preview cropToRect:cropRect];
  return preview;
}

- (UIImage *)mixWithImage:(UIImage *)image {
  CGRect rect = CGRectMake(0, 0, CGImageGetWidth(self.CGImage), CGImageGetHeight(self.CGImage));
  return [self mixWithImage:image rect:rect];
}

- (UIImage *)mixWithImage:(UIImage *)image rect:(CGRect)contentRect
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGSize outputSize = CGSizeMake(w, h);
  UIGraphicsBeginImageContext(outputSize);
  [self drawInRect:CGRectMake(0, 0, w, h)];
  [image drawInRect:contentRect];
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)tintWithColor:(UIColor *)tintColor blendMode:(CGBlendMode)blendMode
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = CGRectMake(0, 0, w, h);
  UIGraphicsBeginImageContext(rect.size);
  [self drawInRect:rect];
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  /// fill color
  CGContextSetFillColorWithColor(context, tintColor.CGColor);
  CGContextSetBlendMode(context, blendMode);
  CGContextFillRect(context, rect);
  
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

- (UIImage *)maskWithImage:(UIImage *)maskImage
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGImageRef maskRef = maskImage.CGImage;
  CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                      CGImageGetHeight(maskRef),
                                      CGImageGetBitsPerComponent(maskRef),
                                      CGImageGetBitsPerPixel(maskRef),
                                      CGImageGetBytesPerRow(maskRef),
                                      CGImageGetDataProvider(maskRef), NULL, false);
  CGImageRef masked = CGImageCreateWithMask(self.CGImage, mask);
  CGImageRelease(mask);
  UIImage *result1 = [UIImage imageWithCGImage:masked];
  CGImageRelease(masked);
  
  UIGraphicsBeginImageContext(CGSizeMake(w, h));
  [result1 drawInRect:CGRectMake(0, 0, w, h)];
  UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return result;
}

CGImageRef copyImageAndRemoveAlphaChannel(CGImageRef imageRef)
{
  CGImageRef retVal = NULL;
  
  NSUInteger w = CGImageGetWidth(imageRef);
  NSUInteger h = CGImageGetHeight(imageRef);
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef offscreenContext = CGBitmapContextCreate(NULL, w, h, 8, 0, colorSpace, kCGImageAlphaNoneSkipLast);
  CGColorSpaceRelease(colorSpace);
  
  if (offscreenContext != NULL)
  {
    CGContextDrawImage(offscreenContext, CGRectMake(0, 0, w, h), imageRef);
    
    retVal = CGBitmapContextCreateImage(offscreenContext);
    CGContextRelease(offscreenContext);
  }
  
  return retVal;
}

- (UIImage *)maskWithImage:(UIImage *)maskImage rect:(CGRect)maskRect
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef context = CGBitmapContextCreate(NULL, maskRect.size.width, maskRect.size.height, 8, 0, colorSpace, kCGImageAlphaPremultipliedLast);
  CGColorSpaceRelease(colorSpace);
  
  if (context == NULL) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = CGRectMake(0, 0, maskRect.size.width, maskRect.size.height);
  UIGraphicsBeginImageContext(rect.size);
  [maskImage drawInRect:rect];
  UIImage *fullMaskImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  CGImageRef maskImageRef = copyImageAndRemoveAlphaChannel(fullMaskImage.CGImage);
  CGContextClipToMask(context, rect, maskImageRef);
  CGContextDrawImage(context, CGRectMake(-rect.origin.x, -(h - rect.origin.y - rect.size.height), w, h), self.CGImage);
  CGImageRelease(maskImageRef);
  
  CGImageRef mainViewContentBitmapContext = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  
  UIImage *result = [UIImage imageWithCGImage:mainViewContentBitmapContext];
  CGImageRelease(mainViewContentBitmapContext);
  
  return result;
}

- (UIImage *)makeGrayscale
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
  CGContextRef context = CGBitmapContextCreate(nil, w, h, 8, 0, colorSpace, kCGImageAlphaNone);
  CGColorSpaceRelease(colorSpace);
  
  if (context == NULL) {
    Log(@"Error!");
    return nil;
  }
  
  CGContextDrawImage(context, rect, self.CGImage);
  
  CGImageRef imageRef = CGBitmapContextCreateImage(context);

  UIImage *result = [UIImage imageWithCGImage:imageRef];
  CGImageRelease(imageRef);
  CGContextRelease(context);
  
  return result;
}

- (UIImage *)makeBlackAndWhiteWithFactor:(float)factor lightestColor:(Byte)lightestColor darkestColor:(Byte)darkestColor
{
  NSInteger w = CGImageGetWidth(self.CGImage);
  NSInteger h = CGImageGetHeight(self.CGImage);
	
  if (w == 0 || h == 0) {
    Log(@"Error!");
		return nil;
	}
	
  CGRect rect = CGRectMake(0, 0, w, h);
	
	NSInteger dataLength = w * h * 4;
	Byte *buffer = (Byte *)malloc(dataLength);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// load image to buffer
	
	CGContextRef context = CGBitmapContextCreate(buffer, w, h, 8, w * 4, colorSpace, kCGImageAlphaPremultipliedLast);
	CGContextClearRect(context, rect);
	CGContextDrawImage(context, rect, self.CGImage);
  
  if (context == NULL) {
    Log(@"Error!");
    return nil;
  }
	
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
  
	// processing
	
  int divColor = (int)(factor * 255.);
	
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++)
    {
    	NSInteger i = (y * w + x) * 4;
      
      int grayColor = (int)(0.30*buffer[i+0] +
                            0.59*buffer[i+1] +
                            0.11*buffer[i+2]);
      
      if (grayColor > divColor) {
        buffer[i+0] = lightestColor;
        buffer[i+1] = lightestColor;
        buffer[i+2] = lightestColor;
        buffer[i+3] = 255;
      }
      else {
        buffer[i+0] = darkestColor;
        buffer[i+1] = darkestColor;
        buffer[i+2] = darkestColor;
        buffer[i+3] = 255;
      }
		}
	}
	
	// create pre-result image from pixel buffer
	
	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, dataLength, NULL);
	
	int bitsPerComponent = 8;
	int bitsPerPixel = 32;
	NSInteger bytesPerRow = 4 * w;
	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaLast;
	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
	CGImageRef resultRef = CGImageCreate(w, h, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
	CGDataProviderRelease(provider);
	UIImage *result1 = [UIImage imageWithCGImage:resultRef];
	CGImageRelease(resultRef);
	
	// get result image from context
	
	UIGraphicsBeginImageContext(CGSizeMake(w, h));
	[result1 drawInRect:CGRectMake(0, 0, w, h)];
	UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	free(buffer);
	
	return result;
}

static void addRoundedRectPathToContext(CGContextRef context, CGRect rect, float cornerWidth, float cornerHeight)
{
  float fw, fh;
  
  if (cornerWidth == 0 || cornerHeight == 0) {
    CGContextAddRect(context, rect);
    return;
  }
  
  CGContextSaveGState(context);
  CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
  CGContextScaleCTM(context, cornerWidth, cornerHeight);
  
  fw = CGRectGetWidth(rect) / cornerWidth;
  fh = CGRectGetHeight(rect) / cornerHeight;
  
  CGContextMoveToPoint(context, fw, fh/2);
  CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
  CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
  CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
  CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
  CGContextClosePath(context);
  CGContextRestoreGState(context);
}

- (UIImage *)makeRoundCornersWithCornerWidth:(float)cornerWidth cornerHeight:(float)cornerHeight
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
  
  CGContextBeginPath(context);
  addRoundedRectPathToContext(context, rect, cornerWidth, cornerHeight);
  CGContextClosePath(context);
  CGContextClip(context);
  
  CGContextDrawImage(context, rect, self.CGImage);
  
  CGImageRef imageMasked = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  CGColorSpaceRelease(colorSpace);
  
  UIImage *result = [UIImage imageWithCGImage:imageMasked];
  CGImageRelease(imageMasked);
  
  return result;
}

- (UIImage *)maskWithPath:(CGPathRef)path
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
  
  CGContextBeginPath(context);
  CGContextAddPath(context, path);
  CGContextClosePath(context);
  CGContextClip(context);
  
  CGContextDrawImage(context, rect, self.CGImage);
  
  CGImageRef imageMasked = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  CGColorSpaceRelease(colorSpace);
  
  UIImage *result = [UIImage imageWithCGImage:imageMasked];
  CGImageRelease(imageMasked);
  
  return result;
}

- (UIImage *)maskFromPath:(CGPathRef)path blur:(CGFloat)blur
{
  NSUInteger w = CGImageGetWidth(self.CGImage);
  NSUInteger h = CGImageGetHeight(self.CGImage);
  
  if (w == 0 || h == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, w, h};
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
  
  CGContextTranslateCTM(context, -w, 0);
  
  /// filling with white color
  CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
  CGContextFillRect(context, rect);
  
  /// draw with black
  CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
  CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
  CGContextSetShadowWithColor(context, CGSizeMake(w, 0), blur, [[UIColor blackColor] CGColor]);
  
  CGContextBeginPath(context);
  CGContextAddPath(context, path);
  CGContextClosePath(context);
  
  CGContextDrawPath(context, kCGPathFill);
  
  /// 
  CGImageRef maskImage = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  CGColorSpaceRelease(colorSpace);
  
  UIImage *result = [UIImage imageWithCGImage:maskImage];
  CGImageRelease(maskImage);
  
  return result;
}

+ (UIImage *)imageWithSize:(CGSize)size fillColor:(UIColor *)color
{
  if (size.width == 0 || size.height == 0) {
    Log(@"Error!");
    return nil;
  }
  
  CGRect rect = {0, 0, size.width, size.height};
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGContextRef context = CGBitmapContextCreate(NULL, size.width, size.height, 8, 4 * size.width, colorSpace, kCGImageAlphaPremultipliedFirst);
  
  /// filling with white color
  CGContextSetFillColorWithColor(context, [color CGColor]);
  CGContextFillRect(context, rect);
  
  ///// draw with black
  //CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
  //CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
  //CGContextSetShadowWithColor(context, CGSizeMake(w, 0), blur, [[UIColor blackColor] CGColor]);
  //
  //CGContextBeginPath(context);
  //CGContextAddPath(context, path);
  //CGContextClosePath(context);
  //
  //CGContextDrawPath(context, kCGPathFill);
  
  ///
  CGImageRef maskImage = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  CGColorSpaceRelease(colorSpace);
  
  UIImage *result = [UIImage imageWithCGImage:maskImage];
  CGImageRelease(maskImage);
  
  return result;
}

@end




