//
//  CLInstagramManager.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLInstagramManager.h"
#import "CLConstantsTypes.h"
#import <AFNetworking/AFNetworking.h>

@implementation CLInstagramManager

- (NSURLRequest *)authorizationURL
{
//    NSString *fullURL = [NSString stringWithFormat:@"%@?client_id=%@&redirect_uri= %@&response_type=token",kInstagramAuthURL, kInstagramClientID, kInstagramRedirectURI];
    NSDictionary * parameters = @{@"redirect_uri" : kInstagramRedirectURI, @"client_id" : kInstagramClientID, @"response_type" : @"token"};
    NSURLRequest *authRequest = (NSURLRequest *)[[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:kInstagramAuthURL parameters:parameters error:nil];
    return authRequest;
}

- (BOOL)receivedValidAccessTokenFromURL:(NSURL *)url
                                  error:(NSError *__autoreleasing *)error
{
    NSURL *appRedirectURL = [NSURL URLWithString:kInstagramRedirectURI];
    if (![appRedirectURL.scheme isEqual:url.scheme] || ![appRedirectURL.host isEqual:url.host])
    {
        NSString *localizedDescription = @"Error while request execution.";
        *error = [NSError errorWithDomain:@"com.instagramkit"
                                     code:0
                                 userInfo:@{NSLocalizedDescriptionKey: localizedDescription}];
        return NO;
    }
    
    BOOL success = YES;
    NSString *token = [self queryStringParametersFromString:url.fragment][@"access_token"];
    if (token)
    {
        self.accessToken = token;
    }
    else
    {
        NSString *localizedDescription = NSLocalizedString(@"Authorization not granted.", @"Error notification to indicate Instagram OAuth token was not provided.");
        *error = [NSError errorWithDomain:@"com.instagramkit"
                                     code:1
                                 userInfo:@{NSLocalizedDescriptionKey: localizedDescription}];
        success = NO;
    }
    return success;
}

- (NSDictionary *)queryStringParametersFromString:(NSString*)string {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [[string componentsSeparatedByString:@"&"] enumerateObjectsUsingBlock:^(NSString * param, NSUInteger idx, BOOL *stop) {
        NSArray *pairs = [param componentsSeparatedByString:@"="];
        if ([pairs count] != 2) return;
        
        NSString *key = [pairs[0] stringByRemovingPercentEncoding];
        NSString *value = [pairs[1] stringByRemovingPercentEncoding];
        [dict setObject:value forKey:key];
    }];
    return dict;
}

@end
