//
//  CLRegistrationModel.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLRegistrationModel.h"

@implementation CLRegistrationModel

- (NSDictionary *)getRegistrationRequestParams
{
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity:3];
    result[@"firstName"] = _firstName;
    result[@"lastName"] = _name;
    result[@"email"] = _email;
    result[@"password"] = _password;
    
    return result;
}

- (NSDictionary *)getLoginRequestParams
{
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity:3];

    result[@"email"] = _email;
    result[@"password"] = _password;
    
    return result;
}

@end
