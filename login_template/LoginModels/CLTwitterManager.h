//
//  CLTwitterManager.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "CLRegistrationModel.h"
#import "CLConstantsTypes.h"

@interface CLTwitterManager : NSObject

@property (nonatomic, copy) RegistrationHandler finishHandler;

- (BOOL)userHasAccessToTwitter;
- (void)signIn;

@end
