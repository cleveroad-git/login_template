//
//  CLRegistrationModel.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLRegistrationModel : NSObject

@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * confirmPassword;

//twitter
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * identifier;
@property (nonatomic, strong) NSString * oAuthToken;
@property (nonatomic, strong) NSString * oAuthTokenSecret;

- (NSDictionary *)getRegistrationRequestParams;
- (NSDictionary *)getLoginRequestParams;

@end
