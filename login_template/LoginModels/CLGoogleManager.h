//
//  CLGoogleManager.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLConstantsTypes.h"
#import <UIKit/UIKit.h>

@interface CLGoogleManager : NSObject

@property (nonatomic, copy) RegistrationHandler finishHandler;
@property (nonatomic, weak) UIViewController * delegate;

- (void)signIn;

@end
