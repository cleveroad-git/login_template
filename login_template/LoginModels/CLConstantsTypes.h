//
//  CLConstants.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLRegistrationModel;

typedef void(^CompletionBlock)(id resObject, NSError *error);
typedef void (^RegistrationHandler)(CLRegistrationModel * model, NSError * error);

FOUNDATION_EXPORT NSString *const TwitterConsumerKey;
FOUNDATION_EXPORT NSString *const TwitterConsumerSecret;

FOUNDATION_EXPORT NSString * const kInstagramAuthURL;
FOUNDATION_EXPORT NSString * const kInstagramApiURl;
FOUNDATION_EXPORT NSString * const kInstagramClientID;
FOUNDATION_EXPORT NSString * const kInstagramClientSecret;
FOUNDATION_EXPORT NSString * const kInstagramRedirectURI;

FOUNDATION_EXPORT NSString * const kGoogleClientId;

FOUNDATION_EXPORT NSString * const kFacebookClientId;

typedef NS_ENUM(NSInteger, LoginType) {
    LoginTypeLogin,
    LoginTypeReg
};