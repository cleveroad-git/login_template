//
//  CLCocialManagerProtocol.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//
#import <Foundation/Foundation.h>

#ifndef CLCocialManagerProtocol_h
#define CLCocialManagerProtocol_h

@protocol CLSocialManager <NSObject>

- (NSURLRequest *)authorizationURL;
- (BOOL)receivedValidAccessTokenFromURL:(NSURL *)url
                                  error:(NSError *__autoreleasing *)error;

@end


#endif /* CLCocialManagerProtocol_h */
