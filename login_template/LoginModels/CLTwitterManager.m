//
//  CLTwitterManager.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLTwitterManager.h"
#import "STTwitter.h"
#import "CLConstantsTypes.h"

typedef void (^accountChooserBlock_t)(ACAccount *account, NSString *errorMessage);

@interface CLTwitterManager ()

@property (atomic, strong) ACAccountStore *accountStore;
@property (atomic, strong) NSMutableArray *accountNames;
@property (atomic, strong) NSArray *accounts;
@property (nonatomic, assign) BOOL permissionsGranted;

@property (nonatomic, strong) STTwitterAPI *twitter;
@property (nonatomic, strong) NSArray *iOSAccounts;
@property (nonatomic, strong) accountChooserBlock_t accountChooserBlock;

@end

@implementation CLTwitterManager

- (BOOL)userHasAccessToTwitter
{
    return [SLComposeViewController
            isAvailableForServiceType:SLServiceTypeTwitter];
}

- (void)connect{
    _accountStore = [[ACAccountStore alloc] init];
    if ([self userHasAccessToTwitter]) {
        
        ACAccountType *twitterAccountType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [_accountStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error){
            
        }];
        _accounts = [_accountStore accountsWithAccountType:twitterAccountType];
        
        if (twitterAccountType.accessGranted) {
            _accountNames = [NSMutableArray new];
            for (ACAccount *accountInStore in _accounts) {
                [_accountNames addObject:accountInStore.username];
            }
            
            //            [Utility actionSheetSelection:_accountNames delegate:self parentView:self.delegate.view];
        }
    }
}

- (void)getInfo
{
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIWithOAuthConsumerName:nil consumerKey:TwitterConsumerKey consumerSecret:TwitterConsumerSecret];
    
    __weak typeof(self) weakSelf = self;
    
    [twitter postReverseOAuthTokenRequest:^(NSString *authenticationHeader) {
        
        self.accountChooserBlock = ^(ACAccount *account, NSString *errorMessage) {
            
            
            STTwitterAPI *twitterAPIOS = [STTwitterAPI twitterAPIOSWithAccount:account];
            
            [twitterAPIOS verifyCredentialsWithUserSuccessBlock:^(NSString *username, NSString *userID) {
                
                [twitterAPIOS postReverseAuthAccessTokenWithAuthenticationHeader:authenticationHeader successBlock:^(NSString *oAuthToken, NSString *oAuthTokenSecret, NSString *userID, NSString *screenName) {
                    
                    CLRegistrationModel * rModel = [[CLRegistrationModel alloc] init];
                    rModel.oAuthToken = oAuthToken;
                    rModel.oAuthTokenSecret = oAuthTokenSecret;
                    rModel.user_id = userID;
                    rModel.name = screenName;
                    if (weakSelf.finishHandler) {
                        weakSelf.finishHandler(rModel, nil);
                    }
                    
                } errorBlock:^(NSError *error) {
                    if (weakSelf.finishHandler) {
                        weakSelf.finishHandler(nil, error);
                    }
                }];
                
            } errorBlock:^(NSError *error) {
                if (weakSelf.finishHandler) {
                    weakSelf.finishHandler(nil, error);
                }
            }];
            
        };
        
        [self chooseAccount];
        
    } errorBlock:^(NSError *error) {
        if (weakSelf.finishHandler) {
            weakSelf.finishHandler(nil, error);
        }
    }];
}

- (void)chooseAccount {
    
    ACAccountType *accountType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreRequestCompletionHandler = ^(BOOL granted, NSError *error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            if(granted == NO) {
                _accountChooserBlock(nil, @"Acccess not granted.");
                return;
            }
            
            self.iOSAccounts = [_accountStore accountsWithAccountType:accountType];
            
            if([_iOSAccounts count] == 1) {
                ACAccount *account = [_iOSAccounts lastObject];
                _accountChooserBlock(account, nil);
            } else {
                UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Select an account:"
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                  destructiveButtonTitle:nil otherButtonTitles:nil];
                for(ACAccount *account in _iOSAccounts) {
                    [as addButtonWithTitle:[NSString stringWithFormat:@"@%@", account.username]];
                }
                [as showInView:[UIApplication sharedApplication].delegate.window];
            }
        }];
    };
    _accountStore = [[ACAccountStore alloc] init];
#if TARGET_OS_IPHONE &&  (__IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0)
    if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_6_0) {
        [self.accountStore requestAccessToAccountsWithType:accountType
                                     withCompletionHandler:accountStoreRequestCompletionHandler];
    } else {
        [self.accountStore requestAccessToAccountsWithType:accountType
                                                   options:NULL
                                                completion:accountStoreRequestCompletionHandler];
    }
#else
    [self.accountStore requestAccessToAccountsWithType:accountType
                                               options:NULL
                                            completion:accountStoreRequestCompletionHandler];
#endif
    
}


- (void)signIn{
    [self getInfo];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex < [_accountNames count]) {
        
        ACAccount *account = _accounts[buttonIndex];
        
        CLRegistrationModel *user = [CLRegistrationModel new];
        
        user.user_id =  [[account valueForKey:@"accountProperties"] objectForKey:@"user_id"];
        user.name =  [[account valueForKey:@"accountProperties"] objectForKey:@"ACPropertyFullName"];
        user.nickname = account.username;
        user.identifier = account.identifier;
        
        //        [IWApp authorizeAccountWithSocialNetwork:user
        //                                            type:@"twitter"
        //                                   withRequester:self.delegate];
    }else{
        //        [Utility alert:@"Please login with any social network you prefer"];
    }
}


@end
