//
//  CLGoogleManager.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLGoogleManager.h"
#import <Google/SignIn.h>
#import "CLConstantsTypes.h"
#import "CLRegistrationModel.h"

@interface CLGoogleManager () <GIDSignInDelegate> {
    GIDSignIn *_signIn;
}

@end

@implementation CLGoogleManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSError* configureError;
        [[GGLContext sharedInstance] configureWithError: &configureError];
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
        
        [GIDSignIn sharedInstance].delegate = self;
        [GIDSignIn sharedInstance].uiDelegate = self;
//        [GIDSignIn sharedInstance].allowsSignInWithWebView = NO;
//        [GIDSignIn sharedInstance].allowsSignInWithBrowser = YES;
    }
    return self;
}

- (void)signIn
{
    _signIn = [GIDSignIn sharedInstance];
//    _signIn.clientID = @"24357836159-b40qaecavbj5ev3jt4kap4h48c8h8k46.apps.googleusercontent.com";
    [_signIn signIn];
    
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString * userId = user.userID;                  // For client-side use only!
    NSString * token = user.authentication.accessToken; // Safe to send to the server
    CLRegistrationModel * rModel = [[CLRegistrationModel alloc] init];
    rModel.oAuthToken = token;
    rModel.user_id = userId;
    _finishHandler(rModel, nil);
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    _finishHandler(nil, error);
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
 
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [_delegate presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [_delegate dismissViewControllerAnimated:YES completion:nil];
}

@end
