//
//  CLInstagramManager.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLSocialManagerProtocol.h"

@interface CLInstagramManager : NSObject <CLSocialManager> {

}

@property (nonatomic, strong) NSString * accessToken;
@property (nonatomic, strong) NSString * userId;

//- (NSURL *)authorizationURl;
//- (BOOL)receivedValidAccessTokenFromURL:(NSURL *)url
//                                  error:(NSError *__autoreleasing *)error;

@end
