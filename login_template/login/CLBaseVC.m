//
//  CLBaseVC.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "CLBaseVC.h"

@interface CLBaseVC ()

@end

@implementation CLBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertWithMessage:(NSString *)message
{
    if (!message) {
        return;
    }
    UIAlertController *loginController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [loginController addAction:okButton];
    [self presentViewController:loginController animated:YES completion:nil];
}

- (void)showPreloader
{
    if (self.bigBackground) {
        return;
    }
    self.bigBackground = [[UIView alloc] initWithFrame:self.view.window.frame];
    self.bigBackground.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    self.bigBackground.alpha = 0.0;
    [self.view.window addSubview:self.bigBackground];
    
    UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.color = [UIColor redColor];
    activityView.frame = CGRectMake((self.bigBackground.frame.size.width - 37.0)/2, (self.bigBackground.frame.size.height - 37.0)/2, 37.0f, 37.0f);
    [self.bigBackground addSubview:activityView];
    [activityView startAnimating];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.bigBackground.alpha = 1.0;
    } completion:nil];
}

- (void)hidePreloader
{
    [self.bigBackground removeFromSuperview];
    self.bigBackground = nil;
}
@end
