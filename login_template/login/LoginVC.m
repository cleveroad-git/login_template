//
//  LoginVC.m
//  Frizby
//
//  Created by Gromov on 29.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "LoginVC.h"
#import "BSKeyboardControls.h"
#import "NSString(common_addition).h"
#import "CLRegistrationModel.h"
#import "LoginView.h"
#import "CLNetworkFacade.h"
#import "AppDelegate.h"

@interface LoginVC () <BSKeyboardControlsDelegate> {
    __weak IBOutlet UIScrollView * scrollView;
    
    __weak IBOutlet UITextField * firstNameTF;
    __weak IBOutlet UITextField * nameTF;
    __weak IBOutlet UITextField * emailTF;
    __weak IBOutlet UITextField * passwordTF;
    __weak IBOutlet UITextField * confirmTF;
    __weak IBOutlet UIButton * loginButton;
    
    __weak IBOutlet UIImageView * firstNameIV;
    __weak IBOutlet UIImageView * nameIV;
    __weak IBOutlet UIImageView * emailIV;
    __weak IBOutlet UIImageView * passwordIV;
    __weak IBOutlet UIImageView * confirmIV;
    
    __weak IBOutlet NSLayoutConstraint * bottomSVConstraint;
    
    BSKeyboardControls *keyboardControls;
    
    NSArray * textFields;
    NSArray * imageViews;
    CLRegistrationModel * registrationModel;
    
    UIImage * tickImage;
    UIImage * crossImage;
    UIView *addView;
}

@end

@implementation LoginVC


- (void)viewDidLoad {
    [super viewDidLoad];
    UIFont * font = [UIFont fontWithName:@"GiddyupStd" size:14.f];
    registrationModel = [[CLRegistrationModel alloc] init];
    tickImage = [UIImage imageNamed:@"Tick"];
    crossImage = [UIImage imageNamed:@"Cross"];
}

- (void)addContentView
{
    NSString * viewname = _loginType == LoginTypeReg ? @"LoginView" : @"LoginView2";
    addView = [[[NSBundle mainBundle] loadNibNamed:viewname owner:self options:nil] firstObject];
    addView.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:addView];
    
    NSDictionary *views = @{@"addView":addView};
    NSDictionary *metrics = @{@"height" : @(CGRectGetHeight(self.view.bounds) - 64), @"width" : @(CGRectGetWidth(self.view.bounds))};
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[addView(height)]|" options:kNilOptions metrics:metrics views:views]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[addView(width)]|" options:kNilOptions metrics:metrics views:views]];
    
    textFields = _loginType == LoginTypeReg ? @[firstNameTF, nameTF, emailTF, passwordTF, confirmTF] : @[emailTF, passwordTF];
    
    keyboardControls = [[BSKeyboardControls alloc] initWithFields:textFields width:CGRectGetWidth(self.view.bounds)];
//    keyboardControls.barTintColor = [FCommonStyles mainThemeColour];
    [keyboardControls setDelegate:self];
    [self setTextFields];
    
    UIImage *i = [loginButton backgroundImageForState:UIControlStateNormal];
    i = [i imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, -10, 0)];
    [loginButton setBackgroundImage:i forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self addContentView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setTextFields
{
    firstNameTF.placeholder = @"Enter your first name";
    nameTF.placeholder = @"Enter your name";
    emailTF.placeholder = @"Enter your email";
    passwordTF.placeholder = @"Enter Password";
    confirmTF.placeholder = @"Confirm Password";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Actions

- (IBAction)regButtonClick:(id)sender
{
    if ([self validate]) {
        [super showPreloader];
        [NETWORK_FACADE registrationWithParams:[registrationModel getRegistrationRequestParams] completionBlock:^(id resObject, NSError *error) {
            [super hidePreloader];
            //
        }];
    }
}

- (IBAction)loginButtonClick:(id)sender
{
    if ([self validate]) {
        [super showPreloader];
        [NETWORK_FACADE loginWithParams:[registrationModel getLoginRequestParams] completionBlock:^(id resObject, NSError *error) {
            [super hidePreloader];
            //
        }];
    }
}

- (BOOL)validate
{
    return _loginType == LoginTypeReg ? [self validateRegistration] : [self validateLogin];
}

- (BOOL)validateLogin
{
    BOOL result = YES;
    
    if (emailTF.text.trim.length && [emailTF.text.trim isEmailValid]) {
        registrationModel.email = emailTF.text;
        emailIV.image = tickImage;
    }
    else {
        emailTF.placeholder = emailTF.text.length ? @"not an email adress" : @"Enter your email";
        emailTF.text = nil;
        emailIV.image = crossImage;
        result = NO;
    }
    
    if (passwordTF.text.trim.length) {
        registrationModel.password = passwordTF.text.trim;
        passwordIV.image = tickImage;
    }
    else {
        passwordIV.image = crossImage;
        result = NO;
    }
    
    return result;
}

- (BOOL)validateRegistration
{
    BOOL result = YES;
    
    if (firstNameTF.text.trim.length) {
        registrationModel.firstName = firstNameTF.text.trim;
        firstNameIV.image = tickImage;
    }
    else {
        [self setNoValidAnimating:firstNameTF];
        firstNameIV.image = crossImage;
        result = NO;
    }
    
    if (nameTF.text.trim.length) {
        registrationModel.name = nameTF.text.trim;
        nameIV.image = tickImage;
    }
    else {
        [self setNoValidAnimating:nameTF];
        nameIV.image = crossImage;
        result = NO;
    }
    
    if (emailTF.text.trim.length && [emailTF.text.trim isEmailValid]) {
        registrationModel.email = emailTF.text;
        emailIV.image = tickImage;
    }
    else {
        [self setNoValidAnimating:emailTF];
        emailTF.placeholder = emailTF.text.length ? @"not an email adress" : @"Enter your email";
        emailTF.text = nil;
        emailIV.image = crossImage;
        result = NO;
    }
    
    if (passwordTF.text.trim.length) {
        registrationModel.password = passwordTF.text.trim;
        passwordIV.image = tickImage;
    }
    else {
        [self setNoValidAnimating:passwordTF];
        passwordIV.image = crossImage;
        result = NO;
    }
    
    if (confirmTF.text.trim.length) {
        registrationModel.confirmPassword = confirmTF.text.trim;
        confirmIV.image = tickImage;
    }
    else {
        [self setNoValidAnimating:confirmTF];
        confirmIV.image = crossImage;
        result = NO;
    }
    
    if (![registrationModel.password isEqualToString:registrationModel.confirmPassword]) {
        result = NO;
        confirmIV.image = crossImage;
    }
    
    return result;
}

- (void)setNoValidAnimating:(UIView *)view
{
    CASpringAnimation * jump = [CASpringAnimation animationWithKeyPath:@"position.x"];
    jump.initialVelocity = 100.0;
    jump.mass = 10.0;
    jump.stiffness = 1500.0;
    jump.damping = 50.0;
    
    jump.fromValue = @(view.layer.position.x + 1.0);
    jump.toValue = @(view.layer.position.x);
    jump.duration = jump.settlingDuration;
    [view.layer addAnimation:jump forKey:nil];
}

#pragma mark -
#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [keyboardControls setActiveField:textField];
}

#pragma mark -
#pragma mark Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [keyboardControls setActiveField:textView];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    [scrollView scrollRectToVisible:field.frame animated:YES];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
    if ([self validate]) {
        [loginButton setBackgroundImage:[UIImage imageNamed:@"Button Blank"] forState:UIControlStateNormal];
    }
}

#pragma mark - NSNotificationCenter

- (void)keyboardWillShowNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrameBegin = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (CGRectEqualToRect(keyboardFrameBegin, keyboardFrameEnd)) {
        return;
    }
    CGFloat height = keyboardFrameEnd.size.height;
    double animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    bottomSVConstraint.constant = height;
    [UIView animateWithDuration:animationDuration delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut animations:^{
                            [self.view layoutIfNeeded];
                        } completion:^(BOOL finished) {
                            NSLog(@"%@", scrollView);
                        }];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrameBegin = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (CGRectEqualToRect(keyboardFrameBegin, keyboardFrameEnd)) {
        return;
    }
    double animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    bottomSVConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

@end
