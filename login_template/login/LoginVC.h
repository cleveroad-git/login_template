//
//  LoginVC.h
//  Frizby
//
//  Created by Gromov on 29.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLBaseVC.h"
#import "CLConstantsTypes.h"


@interface LoginVC : CLBaseVC

@property (nonatomic) LoginType loginType;

@end
