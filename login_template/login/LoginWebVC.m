//
//  LoginWebVC.m
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "LoginWebVC.h"
#import "CLInstagramManager.h"
#import "CLNetworkFacade.h"

@interface LoginWebVC () {
    __weak IBOutlet UIWebView * webView;
}
@end

@implementation LoginWebVC

- (void)dealloc
{
    [webView stopLoading];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    webView.scrollView.bounces = NO;
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    NSURLRequest *authURL = [_manager authorizationURL];
    [webView loadRequest:authURL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSError *error;
    if ([_manager receivedValidAccessTokenFromURL:request.URL error:&error])
    {
        [NETWORK_FACADE getInstagramUser:((CLInstagramManager *)_manager).accessToken completionBlock:^(id resObject, NSError *error) {
            NSLog(@"%@", resObject);
            if (resObject[@"data"][@"id"]) {
                ((CLInstagramManager *)_manager).userId = [NSString stringWithFormat:@"%@", resObject[@"data"][@"id"]];
            }
            [_delegate loginDidFinish:_manager error:error];
        }];
    }
    return YES;
}

@end
