//
//  LoginWebVC.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLSocialManagerProtocol.h"

@protocol LoginWebVCDelegate <NSObject>

- (void)loginDidFinish:(id<CLSocialManager>)manager error:(NSError *)error;

@end

@interface LoginWebVC : UIViewController

@property (nonatomic, strong) id<CLSocialManager> manager;
@property (nonatomic, weak) id<LoginWebVCDelegate> delegate;

@end
