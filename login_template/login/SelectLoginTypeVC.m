//
//  SelectLoginTypeVC.m
//  Frizby
//
//  Created by Gromov on 29.09.15.
//  Copyright © 2015 Gromov. All rights reserved.
//

#import "SelectLoginTypeVC.h"
#import "LoginVC.h"
#import "AppDelegate.h"
#import "CLNetworkFacade.h"
#import "NSString(common_addition).h"

#import "CLTwitterManager.h"
#import "CLInstagramManager.h"
#import "CLGoogleManager.h"

#import "CLConstantsTypes.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKAccessToken.h>


#import "LoginWebVC.h"

@interface SelectLoginTypeVC () <FBSDKLoginButtonDelegate, LoginWebVCDelegate> {
    __weak IBOutlet UIButton * twitterButton;
    __weak IBOutlet UIButton * emailButton;
    __weak IBOutlet FBSDKLoginButton *loginFBButton;
    
    CLTwitterManager * tManager;
}

@end

@implementation SelectLoginTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [loginFBButton setReadPermissions:@[@"public_profile", @"email"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Twitter

- (IBAction)loginViaTwitter
{
    tManager = [[CLTwitterManager alloc] init];
    __weak typeof (self) weakSelf = self;
    tManager.finishHandler = ^(CLRegistrationModel * model, NSError * error) {
        if (model) {
            [NETWORK_FACADE registrationViaTwitterWithSocialId:model.user_id token:model.oAuthToken secret:model.oAuthTokenSecret completionBlock:^(id resObject, NSError *error)
            {
                [weakSelf hidePreloader];
                if (!error) {
                    [self goToMainMenu];
                }
                else
                {
                    [weakSelf showAlertWithMessage:error.localizedDescription];
                }
            }];
        }
        else {
            [weakSelf hidePreloader];
            if (error) {
                [weakSelf showAlertWithMessage:error.localizedDescription];
            }
        }
    };
    if (![tManager userHasAccessToTwitter]) {
        [super showAlertWithMessage:@"Please setup at least one Twitter account in Settings section"];
    }
    else {
        [super showPreloader];
        [tManager signIn];
    }
}

#pragma mark -
#pragma mark Facebook

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if (error) {
        NSLog(@"Process error");
    }
    else if (result.isCancelled)
    {
        NSLog(@"Cancelled");
    }
    else
    {
        NSLog(@"Logged in");
        FBSDKAccessToken *token = [result token];
        [NETWORK_FACADE registrationViaFacebookWithSocialId:token.userID token:token.tokenString completionBlock:^(id resObject, NSError *error)
         {
             if (!error) {
                 [self goToMainMenu];
             }
         }];
    }
    
}

#pragma mark -
#pragma mark Instagram

- (IBAction)loginViaInstagram
{
    CLInstagramManager * manager = [[CLInstagramManager alloc] init];
    LoginWebVC * lwvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginWebVC"];
    lwvc.delegate = self;
    lwvc.manager = manager;
    
    [self presentViewController:lwvc animated:YES completion:nil];
}

#pragma mark -
#pragma mark Google

- (IBAction)loginViaGoogle
{
    CLGoogleManager * manager = [[CLGoogleManager alloc] init];
    manager.delegate = self;
    __weak typeof (self) weakSelf = self;
    manager.finishHandler = ^(CLRegistrationModel * model, NSError * error) {
        if (model) {
            [NETWORK_FACADE registrationViaGoogleWithSocialId:model.user_id token:model.oAuthToken completionBlock:^(id resObject, NSError *error) {
                if (!error) {
                    [self goToMainMenu];
                }
            }];
        }
        else {
            [weakSelf hidePreloader];
            if (error) {
                [weakSelf showAlertWithMessage:error.localizedDescription];
            }
        }
    };
    
    [manager signIn];
}

#pragma mark -
#pragma mark LoginWebVCDelegate

- (void)loginDidFinish:(CLInstagramManager *)manager error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (!error) {
        [NETWORK_FACADE registrationViaInstagramWithSocialId:manager.userId token:manager.accessToken completionBlock:^(id resObject, NSError *error) {
            if (!error) {
                [self goToMainMenu];
            }
        }];
    }
}

- (IBAction)regViaEmail
{
    [self goToLoginVC:LoginTypeReg];
}

- (IBAction)loginViaEmail
{
    [self goToLoginVC:LoginTypeLogin];
}

- (void)goToLoginVC:(LoginType)type
{
    LoginVC * lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    lvc.loginType = type;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (void)goToMainMenu
{
    
}

@end
