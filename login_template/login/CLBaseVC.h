//
//  CLBaseVC.h
//  login_template
//
//  Created by Gromov on 12.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLBaseVC : UIViewController

@property (nonatomic) UIView * bigBackground;

- (void)showAlertWithMessage:(NSString *)message;
- (void)showPreloader;
- (void)hidePreloader;

@end
